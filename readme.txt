Breakfast Order
This project is a web application made using Flask. It is deployed on AWS using gunicorn and nginx. The application allows employees to order breakfast items from a menu and generates an order summary that is emailed to designated recipients.

Dependencies
pip~=23.1.2
Jinja2~=3.1.2
black~=23.3.0
platformdirs~=3.5.1
colorama~=0.4.6
packaging~=23.1
pathspec~=0.11.1
click~=8.1.3
setuptools~=65.5.0
Flask~=2.3.2
pymongo~=4.4.0

Usage
To use the application, the owner of the webpage can decide which menu to show to the employees by changing the value of the key koshary in conditions.json file to "yes" if they want the koshary menu to be shown, or "no" (or anything except "yes") if they want the oriental menu to be shown. also the onwer acn decide when the site is open and closed by changing the corresponding values in the conditions.json file same as the email sending time.

There are several files that can be updated if needed:

employees.json: can be updated to add new employees
koshary_menu.json: can be updated to add new items to the koshary menu
oriental_menu.json: can be updated to add new items to the oriental menu
welcome_message.txt: can be updated to show a message on the home page to the employees, such as "Today's menu is koshary".
Employees can use the webpage only from 6 AM GMT+3 to 9:30 AM GMT+3. The home page allows employees to choose their name, and then they can go to the order page, where they can choose multiple items from any category and submit their order. The total order price will be shown for them, and their order will be sent to MongoDB for later gathering and preparing it for the email. Also, the employee name is added to their corresponding order to be added to orders.txt.

After 9:30 AM GMT+3, the application will wait for approximately 1 minutes, and then it will send an email to the recipients listed in recipients.json. The email will contain the content of message.txt, the total order, and the order for each employee with the order price for them.


note that for the oriental menu by default delivery is set to 3 EGP and for the koshry it is 2.5
