FROM python:3.9-alpine3.13

ENV PYTHONUNBUFFERED 1

COPY ./ /breakfast_order

WORKDIR /breakfast_order

RUN pip install --upgrade pip

RUN pip install -r requirements.txt


EXPOSE 5000