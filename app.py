import datetime
import time
from flask import Flask, render_template, request
import smtplib
from email.mime.text import MIMEText
from pymongo import MongoClient
from pymongo.errors import PyMongoError
from pymongo.server_api import ServerApi
import threading
import json


app = Flask(__name__)


def connect_to_database():
    # used to connect to the database
    try:
        uri = "mongodb+srv://<username>@<cluster>.mongodb.net/?retryWrites=true&w=majority"
        client = MongoClient(uri, server_api=ServerApi("1"))
        mydb = client["Cluster0"]
        mycol = mydb["orders"]
        return mycol
    except KeyError:
        raise ValueError("MONGODB_URI environment variable not set")


mycol = connect_to_database()


def put_order_to_database(cart):
    # takes the order from user and insert it to the database
    try:
        result = mycol.insert_many(cart)
        print(f"{len(result.inserted_ids)} documents inserted into the database.")
    except PyMongoError as e:
        print(f"Error inserting documents into the database: {e}")


def handle_order(cart):
    # handels the oredr after it is submitted by the user to be ready to be inserted into orders.txt file
    order = []
    order.extend(list(cart[-2].keys()))
    order.append(str(cart[-1]))
    for DICT in cart[:-2]:
        try:
            order.append(
                f"{DICT['count']} {DICT['item']} {DICT['comment']} {DICT['orderType']}"
            )
        except:
            order.append(f"{DICT['count']} {DICT['item']}")
        
    return order


def prepare_order_for_sending():
    # retriev the order from the database and handle it for sending
    try:
        orders_collection = mycol.find({}, {"_id": 0})
        dict_of_orders = {}
        for document in orders_collection:
            try:
                key = (document["comment"], document["orderType"], document["item"])
            except:
                key = document["item"]
            count = document["count"]
            if key in dict_of_orders:
                dict_of_orders[key]["count"] += count
            else:
                dict_of_orders[key] = document
        list_of_orders = list(dict_of_orders.values())
        final_order = []
        for DICT in list_of_orders:
            try:
                final_order.append(
                    f"{DICT['count']} {DICT['item']} {DICT['orderType']} {DICT['comment']}"
                )
            except:
                final_order.append(f"{DICT['count']} {DICT['item']}")
        final_order = "\n".join(final_order)
        prepare_the_email(final_order)
    except PyMongoError as e:
        print(f"Error retrieving orders from database: {e}")


def prepare_the_email(final_order):
    # prepare the full message that will be send and send it if it is not the weekend
    sender = "<email_address>"
    with open("recipients.json", "r", encoding="UTF-8") as recipients_file:
        recipients = json.loads(recipients_file.read())
    subject = "Breakfast Order"
    with open("message.txt", "r", encoding="UTF-8") as message_file:
        message = message_file.read()

    with open("orders.txt", "r", encoding="UTF-8") as orders_file:
        lines = orders_file.readlines()
    sorted_lines = sorted(lines)
    with open("orders.txt", "w", encoding="UTF-8") as orders_file:
        orders_file.writelines(sorted_lines)

    with open("orders.txt", "r", encoding="UTF-8") as orders_file:
        orders = orders_file.read()
    body = f"{message}\n{final_order}\n\n{orders}"
    date = datetime.date.today()
    today = date.strftime("%A")
    with open("conditions.json", "r", encoding="UTF-8") as conditions_file:
        conditions = json.loads(conditions_file.read())
    if today not in conditions["weekend"]:
        for recipient in recipients:
            send_email(subject, body, sender, recipient)
            time.sleep(5)


def send_email(subject, body, sender, recipient):
    # send the email to the recipients in the recipients.json file
    msg = MIMEText(body)
    msg["Subject"] = subject
    msg["From"] = sender
    msg["To"] = recipient
    try:
        #######smtplib.SMTP will work with hotmail.com need to be chenged if using any thing else#####
        with smtplib.SMTP("smtp.office365.com", 587) as server:
            server.starttls()
            server.login(sender, "<password>")
            time.sleep(5)
            server.sendmail(sender, recipient, msg.as_string())
            server.quit()
        print("Email sent successfully")
    except Exception as e:
        print(f"Error sending email: {e}")


def schedule_sending_the_order():
    # check the time for sending the order and delete the database and order.txt after 10 mins from sending the order
    while True:
        with open("conditions.json", "r", encoding="UTF-8") as conditions_file:
            conditions = json.loads(conditions_file.read())
        start_time = datetime.time(
            conditions["email start hour"], conditions["email start minute"], 0
        )
        end_time = datetime.time(
            conditions["email end hour"], conditions["email end minute"], 0
        )
        now = datetime.datetime.now().time()
        if start_time <= now <= end_time:
            time.sleep(10)
            prepare_order_for_sending()
            time.sleep(600)
            mycol.delete_many({})
            with open("orders.txt", "w", encoding="UTF-8") as orders_file:
                orders_file.write("")
        time.sleep(60)


schedule_sending_the_order_thread = threading.Thread(target=schedule_sending_the_order)
schedule_sending_the_order_thread.start()


def check_time(func):
    # used to render the time out html if it is past 9:30 AM or it is weekend
    def wrapper():
        with open("conditions.json", "r", encoding="UTF-8") as conditions_file:
            conditions = json.loads(conditions_file.read())
        start_time = datetime.time(
            conditions["site start hour"], conditions["site start minute"], 0
        )
        end_time = datetime.time(
            conditions["site end hour"], conditions["site end minute"], 0
        )
        now = datetime.datetime.now().time()
        date = datetime.date.today()
        today = date.strftime("%A")
        with open("conditions.json", "r", encoding="UTF-8") as conditions_file:
            conditions = json.loads(conditions_file.read())
        if start_time <= now <= end_time:
            if today in conditions["weekend"]:
                return render_template("time_out.html")
        else:
            return render_template("time_out.html")
        return func()

    return wrapper


def koshary_check(func):
    # used to render the koshary menu if the owner change the condition in the conditions file
    def wrapper():
        with open("conditions.json", "r", encoding="UTF-8") as conditions_file:
            conditions = json.loads(conditions_file.read())
        if conditions["koshary"] == "yes":
            with open("employees.json", "r", encoding="UTF-8") as employees_file:
                employees = json.loads(employees_file.read())
            employee = request.args.get("employee")
            with open("koshary_menu.json", "r", encoding="UTF-8") as koshary_file:
                koshary_menu = json.loads(koshary_file.read())
            return render_template(
                "order_koshary.html",
                employee=employee,
                menu=koshary_menu,
                employees=employees,
            )
        else:
            pass
        return func()

    return wrapper


@app.route("/", endpoint="index")
@check_time
def index():
    with open("employees.json", "r", encoding="UTF-8") as employees_file:
        employees = json.loads(employees_file.read())
    with open("welcome_message.txt", "r", encoding="UTF-8") as welcome_message_file:
        welcome_message = welcome_message_file.read()
    return render_template(
        "index.html", employees=employees, welcome_message=welcome_message
    )


@app.route("/order/", methods=["GET", "POST"], endpoint="order")
@check_time
@koshary_check
def order_selection():
    with open("oriental_menu.json", "r", encoding="UTF-8") as oriental_file:
        oriental_menu = json.loads(oriental_file.read())
    with open("employees.json", "r", encoding="UTF-8") as employees_file:
        employees = json.loads(employees_file.read())
    employee = request.args.get("employee")
    return render_template(
        "order_oriental.html",
        employee=employee,
        menu=oriental_menu,
        employees=employees,
    )


@app.route("/submit-order", methods=["POST", "GET"])
@check_time
def submit_order():
    with open("employees.json", "r", encoding="UTF-8") as employees_file:
        employees = json.loads(employees_file.read())
    data = request.get_json()
    cart = data["cart"]
    total = data["total"]
    put_order_to_database(cart)
    employee = request.args.get("employee")
    cart += [{}, ""]
    cart[-2][employee] = employees[employee]
    cart[-1] = total
    order = handle_order(cart)
    with open("orders.txt", "a", encoding="UTF-8") as orders_file:
        orders_file.write(",".join(order) + f"\n")
    return "Order submitted successfully!"


if __name__ == "__main__":
    app.run()

#######  sudo systemctl restart breakfast_order >>>>>>>>>>>>>>>>> used to restart the service
#####    sudo systemctl restart nginx >>>>>>>>>>>>>>>>> used to restart nginx

#######  sudo journalctl -f -u breakfast_order >>>>>>>>>> used to live logs

#####sudo journalctl -u breakfast_order >>>>>> used to show the full logs
